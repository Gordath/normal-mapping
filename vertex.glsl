attribute vec3 attr_tangent;
varying vec3 tan_ldir,tan_vdir;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
	gl_TexCoord[0]=gl_MultiTexCoord0;
	
	vec3 vpos=(gl_ModelViewMatrix*gl_Vertex).xyz;
	vec3 normal=gl_NormalMatrix*gl_Normal;
	vec3 tangent=gl_NormalMatrix*attr_tangent;
	vec3 binormal=cross(normal,tangent);
	
	mat3 TBN=mat3(tangent.x,binormal.x,normal.x,
					tangent.y,binormal.y,normal.y,
					tangent.z,binormal.z,normal.z);
	
	tan_ldir=TBN*(gl_LightSource[0].position.xyz-vpos);
	tan_vdir=TBN*(-vpos);
}