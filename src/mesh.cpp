#include <stdio.h>
#include "objfile.h"
#include "opengl.h"
#include "mesh.h"

Mesh::Mesh()
{
	vertices = 0;
	vcount = 0;
}

Mesh::Mesh(int vcount)
{
	vertices = 0;
	vcount = 0;

	set_vertex_count(vcount);
}

Mesh::~Mesh()
{
	delete [] vertices;
}

void Mesh::set_vertex_count(int vcount)
{
	delete [] vertices;

	if(vcount) {
		vertices = new Vertex[vcount];
	} else {
		vertices = 0;
	}
	this->vcount = vcount;
}

int Mesh::get_vertex_count() const
{
	return vcount;
}

Vertex *Mesh::get_vertex_array() const
{
	return vertices;
}

bool Mesh::load(const char *fname)
{
	objfile *objf = objf_load(fname);
	if(!objf) {
		fprintf(stderr, "failed to load obj file: %s\n", fname);
		return false;
	}

	vcount = objf_vertex_count(objf);

	// allocate storage for vcount vertices
	set_vertex_count(vcount);

	for(int i=0; i<vcount; i++) {
		float *v = objf_vertex(objf, i);
		vertices[i].pos.x = v[0];
		vertices[i].pos.y = v[1];
		vertices[i].pos.z = v[2];

		float *n = objf_normal(objf, i);
		vertices[i].normal.x = n[0];
		vertices[i].normal.y = n[1];
		vertices[i].normal.z = n[2];

		float *tc = objf_texcoord(objf, i);
		vertices[i].texcoord[0] = tc[0];
		vertices[i].texcoord[1] = tc[1];
	}

	objf_free(objf);
	return true;
}

extern int attr_tangent;

void Mesh::draw() const
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &vertices->pos);
	glNormalPointer(GL_FLOAT, sizeof(Vertex), &vertices->normal);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), vertices->texcoord);

	if(attr_tangent != -1)
	{
		glEnableVertexAttribArray(attr_tangent);
		glVertexAttribPointer(attr_tangent,3,GL_FLOAT,GL_FALSE,sizeof(Vertex),&vertices->tangent);
	}

	glDrawArrays(GL_TRIANGLES, 0, vcount);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}


void Mesh::draw_normals() const
{
	glBegin(GL_LINES);
	glColor3f(0, 0, 1);

	for(int i=0; i<vcount; i++) {
		Vector3 v = vertices[i].pos;
		Vector3 n = vertices[i].normal;

		glVertex3f(v.x, v.y, v.z);
		glVertex3f(v.x + n.x, v.y + n.y, v.z + n.z);
	}

	glEnd();
}

void Mesh::draw_tangents() const
{
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);

	for(int i=0; i<vcount; i++) {
		Vector3 v = vertices[i].pos;
		Vector3 n = vertices[i].tangent;

		glVertex3f(v.x, v.y, v.z);
		glVertex3f(v.x + n.x, v.y + n.y, v.z + n.z);
	}

	glEnd();
}
