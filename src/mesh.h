#ifndef MESH_H_
#define MESH_H_

#include "vec.h"

struct Vertex {
	Vector3 pos;
	Vector3 normal;
	Vector3 tangent;
	float texcoord[2];
};

class Mesh {
private:
	int vcount;
	Vertex *vertices;

public:
	Mesh();
	Mesh(int vcount);
	~Mesh();

	void set_vertex_count(int vcount);
	int get_vertex_count() const;

	Vertex *get_vertex_array() const;

	bool load(const char *fname);

	void draw() const;
	void draw_normals() const;
	void draw_tangents() const;
};

#endif	// MESH_H_
