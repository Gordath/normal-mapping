#ifndef OBJFILE_H_
#define OBJFILE_H_

struct objfile;

#ifdef __cplusplus
extern "C" {
#endif

struct objfile *objf_load(const char *fname);
void objf_free(struct objfile *obj);

int objf_vertex_count(struct objfile *obj);
int objf_face_count(struct objfile *obj);

float *objf_vertices(struct objfile *obj);
float *objf_normals(struct objfile *obj);
float *objf_texcoords(struct objfile *obj);

float *objf_vertex(struct objfile *obj, int idx);
float *objf_normal(struct objfile *obj, int idx);
float *objf_texcoord(struct objfile *obj, int idx);


#ifdef __cplusplus
}
#endif

#endif	/* OBJFILE_H_ */
