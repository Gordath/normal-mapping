/* version 2: does not crash when there are no texture coordinates or normals */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>
#include "objfile.h"

#define INVALID_IDX		INT_MIN

struct objfile {
	float *verts, *normals, *texcoords;
	int num_faces;
};

struct vec3 {
	float x, y, z;
};

struct face {
	int vidx[4];
	int nidx[4];
	int tidx[4];
};

static int objf_read(struct objfile *obj, FILE *fp, const char *fname);
static int count_elem(FILE *fp, const char *elem);
static char *strip(char *s);
static int mkface(char *line, struct face *face);
static int parse_face(char *s, int *vidx, int *nidx, int *tidx);
static int is_int(const char *str);


struct objfile *objf_load(const char *fname)
{
	FILE *fp;
	struct objfile *obj;

	if(!(fp = fopen(fname, "rb"))) {
		fprintf(stderr, "objf_load: failed to open file: %s: %s\n", fname, strerror(errno));
		return 0;
	}

	if(!(obj = malloc(sizeof *obj))) {
		perror("failed to allocate objfile structure\n");
		fclose(fp);
		return 0;
	}

	if(objf_read(obj, fp, fname) == -1) {
		free(obj);
		obj = 0;
	}
	fclose(fp);
	return obj;
}

void objf_free(struct objfile *obj)
{
	if(obj) {
		free(obj->verts);
		free(obj->normals);
		free(obj->texcoords);
		free(obj);
	}
}

int objf_vertex_count(struct objfile *obj)
{
	return obj->num_faces * 3;
}

int objf_face_count(struct objfile *obj)
{
	return obj->num_faces;
}

float *objf_vertices(struct objfile *obj)
{
	return obj->verts;
}

float *objf_normals(struct objfile *obj)
{
	return obj->normals;
}

float *objf_texcoords(struct objfile *obj)
{
	return obj->texcoords;
}


float *objf_vertex(struct objfile *obj, int idx)
{
	return obj->verts + idx * 3;
}

float *objf_normal(struct objfile *obj, int idx)
{
	return obj->normals + idx * 3;
}

float *objf_texcoord(struct objfile *obj, int idx)
{
	return obj->texcoords + idx * 2;
}

static int objf_read(struct objfile *obj, FILE *fp, const char *fname)
{
	int i, j, res = -1;
	int vcount, ncount, tcount, fcount;
	int num_verts;
	struct vec3 *varr, *narr, *tarr;
	struct vec3 *vptr, *nptr, *tptr;
	struct vec3 dummy_vec = {0, 0, 0};
	struct face *faces, *fptr;
	float *vdest, *ndest, *tdest;
	char buf[512];

	vcount = count_elem(fp, "v");
	rewind(fp);
	ncount = count_elem(fp, "vn");
	rewind(fp);
	tcount = count_elem(fp, "vt");
	rewind(fp);
	fcount = count_elem(fp, "f");
	rewind(fp);

	obj->num_faces = fcount;

	if(!vcount || !fcount) {
		fprintf(stderr, "invalid or corrupted obj file: %s\n", fname);
		return -1;
	}

	printf("Reading obj file: %s (%d vertices, %d faces)\n", fname, vcount, fcount);

	vptr = varr = malloc(vcount * sizeof *varr);
	nptr = narr = ncount ? malloc(ncount * sizeof *narr) : &dummy_vec;
	tptr = tarr = tcount ? malloc(tcount * sizeof *tarr) : &dummy_vec;
	fptr = faces = malloc(fcount * sizeof *faces);

	if(!varr || !narr || !tarr || !faces) {
		perror("can't allocate enough memory for the geometry");
		goto cleanup;
	}

	while(fgets(buf, sizeof buf, fp)) {
		char *line;

		if(!(line = strip(buf)) || *line == '#') {
			continue;	/* ignore empty lines and comments */
		}

		if(sscanf(line, "v %f %f %f", &vptr->x, &vptr->y, &vptr->z) == 3) {
			vptr++;
		} else if(sscanf(line, "vn %f %f %f", &nptr->x, &nptr->y, &nptr->z) == 3) {
			nptr++;
		} else if(sscanf(line, "vt %f %f", &tptr->x, &tptr->y) == 2) {
			tptr++;
		} else if(mkface(line, fptr) == 0) {
			fptr++;
		}
	}

	/* now go forth and create the straight-up 3-vert-per-face vertex arrays */
	num_verts = obj->num_faces * 3;

	vdest = obj->verts = malloc(num_verts * 3 * sizeof *obj->verts);
	ndest = obj->normals = malloc(num_verts * 3 * sizeof *obj->normals);
	tdest = obj->texcoords = malloc(num_verts * 2 * sizeof *obj->texcoords);

	if(!obj->verts || !obj->normals || !obj->texcoords) {
		free(obj->verts);
		free(obj->normals);
		goto cleanup;
	}

	/* for all faces */
	for(i=0; i<fcount; i++) {
		/* for the 3 vertices of each face */
		for(j=0; j<3; j++) {
			*vdest++ = varr[faces[i].vidx[j]].x;
			*vdest++ = varr[faces[i].vidx[j]].y;
			*vdest++ = varr[faces[i].vidx[j]].z;

			*ndest++ = narr[faces[i].nidx[j]].x;
			*ndest++ = narr[faces[i].nidx[j]].y;
			*ndest++ = narr[faces[i].nidx[j]].z;

			*tdest++ = tarr[faces[i].tidx[j]].x;
			*tdest++ = tarr[faces[i].tidx[j]].y;
		}
	}
	res = 0;	/* success */

cleanup:
	free(varr);
	if(narr != &dummy_vec) {
		free(narr);
	}
	if(tarr != &dummy_vec) {
		free(tarr);
	}
	free(faces);
	return res;
}

static int count_elem(FILE *fp, const char *elem)
{
	int count = 0;
	char buf[512];

	while(fgets(buf, sizeof buf, fp)) {
		if(buf[0] == elem[0] && (!elem[1] || buf[1] == elem[1])) {
			count++;
		}
	}
	return count;
}


static char *strip(char *s)
{
	while(*s && isspace(*s)) {
		s++;
	}
	return s;
}

static int mkface(char *s, struct face *face)
{
	int nverts;

	if(s[0] != 'f') {
		return -1;
	}

	if((nverts = parse_face(s + 2, face->vidx, face->nidx, face->tidx)) == -1) {
		return -1;
	}
	if(nverts > 3) {
		fprintf(stderr, "warning face with more than 3 vertices found\n");
	}
	return 0;
}

#define SEP	" \t\v\n\r"
/* returns the number of vertices on this face, or -1 on error. */
static int parse_face(char *s, int *vidx, int *nidx, int *tidx)
{
	int i, num_verts = 0;
	char *tok[] = {0, 0, 0, 0};
	char *subtok;

	for(i=0; i<4; i++) {
		if((!(tok[i] = strtok(i == 0 ? s : 0, SEP)) || !is_int(tok[i]))) {
			if(i < 3) return -1;
		} else {
			num_verts++;
		}
	}

	for(i=0; i<4; i++) {
		subtok = tok[i];
		if(!subtok || !*subtok || !is_int(subtok)) {
			if(i < 3) {
				return -1;
			}
			vidx[i] = INVALID_IDX;
		} else {
			vidx[i] = atoi(subtok);
			if(vidx[i] > 0) vidx[i]--;	/* convert to 0-based */
		}

		while(subtok && *subtok && *subtok != '/') {
			subtok++;
		}
		if(subtok && *subtok && *++subtok && is_int(subtok)) {
			tidx[i] = atoi(subtok);
			if(tidx[i] > 0) tidx[i]--;	/* convert to 0-based */
		} else {
			tidx[i] = 0;
		}

		while(subtok && *subtok && *subtok != '/') {
			subtok++;
		}
		if(subtok && *subtok && *++subtok && is_int(subtok)) {
			nidx[i] = atoi(subtok);
			if(nidx[i] > 0) nidx[i]--;	/* convert to 0-based */
		} else {
			nidx[i] = 0;
		}
	}

	return num_verts;
}


static int is_int(const char *str)
{
	return isdigit(str[0]) ||
		(str[0] == '-' && isdigit(str[1])) ||
		(str[0] == '+' && isdigit(str[1]));
}
