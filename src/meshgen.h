#ifndef MESHGEN_H_
#define MESHGEN_H_

#include "mesh.h"

Mesh *create_sphere(float rad, int useg, int vseg);
//Mesh *create_torus(float tor_rad, float ring_rad, int useg, int vseg);

#endif	// MESHGEN_H_
