#define _USE_MATH_DEFINES
#include <math.h>
#include "meshgen.h"

static Vector3 sphvec(float theta, float phi)
{
	Vector3 res;
	res.x = sin(theta) * sin(phi);
	res.y = cos(phi);
	res.z = -cos(theta) * sin(phi);
	return res;
}

#define UANGLE(u)	(2.0 * M_PI * (u))
#define VANGLE(v)	(M_PI * (v))
static Vertex sphere_vertex(float rad, float u, float v)
{
	Vector3 vpos = sphvec(UANGLE(u), VANGLE(v));

	Vertex vert;
	vert.pos = vpos * rad;
	vert.normal = vpos;
	vert.texcoord[0] = u;
	vert.texcoord[1] = 1.0 - v;

	vert.tangent=sphvec(UANGLE(u+0.1),VANGLE(0.5))-sphvec(UANGLE(u -0.1),VANGLE(0.5));
	vert.tangent.normalize();

	return vert;
}

Mesh *create_sphere(float rad, int useg, int vseg)
{
	if(useg < 4) useg = 4;
	if(vseg < 3) vseg = 3;

	int num_faces = useg * (vseg - 1);
	int num_verts = num_faces * 6;	// two triangles/face three verts each

	Mesh *mesh = new Mesh;
	mesh->set_vertex_count(num_verts);
	Vertex *varr = mesh->get_vertex_array();

	float du = 1.0 / (float)useg;
	float dv = 1.0 / (float)(vseg - 1);

	float u = 0.0;
	for(int i=0; i<useg; i++) {
		float v = 0.0;
		for(int j=0; j<vseg - 1; j++) {
			*varr++ = sphere_vertex(rad, u, v);
			*varr++ = sphere_vertex(rad, u + du, v);
			*varr++ = sphere_vertex(rad, u + du, v + dv);

			*varr++ = sphere_vertex(rad, u, v);
			*varr++ = sphere_vertex(rad, u + du, v + dv);
			*varr++ = sphere_vertex(rad, u, v + dv);

			v += dv;
		}
		u += du;
	}

	return mesh;
}
