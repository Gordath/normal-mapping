#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "opengl.h"
#include "meshgen.h"
#include "sdr.h"
#include "image.h"

static bool init();
static void display();
static void reshape(int x, int y);
static void keyboard(unsigned char key, int x, int y);
static void mouse(int bn, int state, int x, int y);
static void motion(int x, int y);
static unsigned int load_texture(const char *fname);

static unsigned int normalmap,difftex;
static unsigned int sdr;
int attr_tangent;

static float cam_theta, cam_phi = 20, cam_dist = 8;
static Mesh *mesh;
static bool show_normals, show_tangents;

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitWindowSize(800, 600);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutCreateWindow("normal mapping example");

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	if(!init()) {
		system("pause");
		return 1;
	}

	glutMainLoop();
	return 0;
}

static bool init()
{
	glewInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	if(!(mesh = create_sphere(2.0, 16, 10))) {
		fprintf(stderr, "failed to create sphere\n");
		return false;
	}

	if(!(sdr=create_program_load("vertex.glsl","pixel.glsl")))
	{
		return false;
	}
	glUseProgram(sdr);

	attr_tangent=glGetAttribLocation(sdr,"attr_tangent");

	if(!(normalmap=load_texture("data/stonewall2_normal.tga")))
	{
		return false;
	}
	if(!(difftex=load_texture("data/stonewall2_diffuse.tga")))
	{
		return false;
	}

	return true;
}

static void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, 0, -cam_dist);
	glRotatef(cam_phi, 1, 0, 0);
	glRotatef(cam_theta, 0, 1, 0);

	float lpos[] = {-10, 10, 15, 1.0};
	glLightfv(GL_LIGHT0, GL_POSITION, lpos);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D,normalmap);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,difftex);

	glUseProgram(sdr);
	glUniform1i(glGetUniformLocation(sdr,"normalmap"),1);
	glUniform1i(glGetUniformLocation(sdr,"difftex"),0);

	mesh->draw();

	glUseProgram(0);
	glDisable(GL_LIGHTING);
	if(show_normals) {
		mesh->draw_normals();
	}
	if(show_tangents) {
		mesh->draw_tangents();
	}
	glEnable(GL_LIGHTING);

	glutSwapBuffers();
	assert(glGetError() == GL_NO_ERROR);
}

static void reshape(int x, int y)
{
	glViewport(0, 0, x, y);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (float)x / (float)y, 0.5, 500.0);
}

static void keyboard(unsigned char key, int x, int y)
{
	switch(key) {
	case 27:
		exit(0);

	case 'n':
		show_normals = !show_normals;
		glutPostRedisplay();
		break;

	case 't':
		show_tangents = !show_tangents;
		glutPostRedisplay();
		break;

	case 'w':
		{
			static bool wireframe;
			wireframe = !wireframe;

			if(wireframe) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			} else {
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}
			glutPostRedisplay();
		}
		break;

	default:
		break;
	}
}

static bool bnstate[32];
static int prev_x, prev_y;

static void mouse(int bn, int state, int x, int y)
{
	prev_x = x;
	prev_y = y;

	bnstate[bn - GLUT_LEFT_BUTTON] = state == GLUT_DOWN;
}

static void motion(int x, int y)
{
	int dx = x - prev_x;
	int dy = y - prev_y;
	prev_x = x;
	prev_y = y;

	if(bnstate[0]) {
		cam_theta += dx * 0.5;
		cam_phi += dy * 0.5;

		if(cam_phi < -90) cam_phi = -90;
		if(cam_phi > 90) cam_phi = 90;
		glutPostRedisplay();
	}
	if(bnstate[2]) {
		cam_dist += dy * 0.1;

		if(cam_dist < 0) cam_dist = 0;
		glutPostRedisplay();
	}
}

static unsigned int load_texture(const char *fname)
{
	struct image *img = load_image(fname);
	if(!img) {
		return 0;
	}

	unsigned int tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->width, img->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->pixels);
	free_image(img);

	return tex;
}
