src = $(wildcard src/*.cpp)
csrc = $(wildcard src/*.c)
obj = $(src:.cpp=.o) $(csrc:.c=.o)
dep = $(obj:.o=.d)
bin = norm

CFLAGS = -pedantic -Wall -g
CXXFLAGS = $(CFLAGS)
LDFLAGS = $(libgl) -lm

ifeq ($(shell uname -s), Darwin)
	libgl = -framework OpenGL -framework GLUT -lGLEW
else
	libgl = -lGL -lGLU -lglut -lGLEW
endif

$(bin): $(obj)
	$(CXX) -o $@ $(obj) $(LDFLAGS)

-include $(dep)

.c.d:
	@$(CPP) $(CFLAGS) $< -MM -MT $(@:.d=.o) >$@

.cpp.d:
	@$(CPP) $(CXXFLAGS) $< -MM -MT $(@:.d=.o) >$@

.PHONY: clean
clean:
	rm -f $(obj) $(bin) $(dep)
