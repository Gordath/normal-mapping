uniform sampler2D normalmap;
uniform sampler2D diff_tex;
varying vec3 tan_ldir,tan_vdir;


void main()
{

	vec3 normtex=texture2D(normalmap,gl_TexCoord[0].xy).xyz*2.0-1.0;
	vec3 diff_tex=texture2D(diff_tex,gl_TexCoord[0].xy).xyz;
	
	vec3 v=normalize(tan_vdir);
	vec3 l=normalize(tan_ldir);
	vec3 n=normalize(normtex);
	
	
	vec3 h=normalize(v+l);
	float ndoth=max(dot(n,h),0.0);
	float blinnSpeccLight=pow(ndoth,60.0);
	//vec3 Reflection=reflect(tan_ldir,normtex); //mono sto phong
	
	float diffLight=max(dot(n,l),0.0);
	vec3 diffColor=diffLight*diff_tex.xyz;
	
	gl_FragColor.rgb=diffColor+vec3(1.0,1.0,1.0)*blinnSpeccLight;
	gl_FragColor.a=1.0;
}